# -*- coding: utf-8 -*-
from max.theme.content.todo_folder import ITodoFolder  # NOQA E501
from max.theme.testing import MAX_THEME_INTEGRATION_TESTING  # noqa
from plone import api
from plone.api.exc import InvalidParameterError
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


try:
    from plone.dexterity.schema import portalTypeToSchemaName
except ImportError:
    # Plone < 5
    from plone.dexterity.utils import portalTypeToSchemaName


class TodoFolderIntegrationTest(unittest.TestCase):

    layer = MAX_THEME_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])

    def test_ct_todo_folder_schema(self):
        fti = queryUtility(IDexterityFTI, name='Todo Folder')
        schema = fti.lookupSchema()
        self.assertEqual(ITodoFolder, schema)

    def test_ct_todo_folder_fti(self):
        fti = queryUtility(IDexterityFTI, name='Todo Folder')
        self.assertTrue(fti)

    def test_ct_todo_folder_factory(self):
        fti = queryUtility(IDexterityFTI, name='Todo Folder')
        factory = fti.factory
        obj = createObject(factory)

        self.assertTrue(
            ITodoFolder.providedBy(obj),
            u'ITodoFolder not provided by {0}!'.format(
                obj,
            ),
        )

    def test_ct_todo_folder_adding(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        obj = api.content.create(
            container=self.portal,
            type='Todo Folder',
            id='todo_folder',
        )

        self.assertTrue(
            ITodoFolder.providedBy(obj),
            u'ITodoFolder not provided by {0}!'.format(
                obj.id,
            ),
        )

    def test_ct_todo_folder_globally_addable(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        fti = queryUtility(IDexterityFTI, name='Todo Folder')
        self.assertTrue(
            fti.global_allow,
            u'{0} is not globally addable!'.format(fti.id)
        )

    def test_ct_todo_folder_filter_content_type_true(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        fti = queryUtility(IDexterityFTI, name='Todo Folder')
        portal_types = self.portal.portal_types
        parent_id = portal_types.constructContent(
            fti.id,
            self.portal,
            'todo_folder_id',
            title='Todo Folder container',
         )
        self.parent = self.portal[parent_id]
        with self.assertRaises(InvalidParameterError):
            api.content.create(
                container=self.parent,
                type='Document',
                title='My Content',
            )
